package com.melissa.test.service.impl;

import com.melissa.test.entity.CreditCard;
import com.melissa.test.exceptions.CreditCardTypeNotFoundException;
import com.melissa.test.exceptions.InvalidCardException;
import com.melissa.test.exceptions.MultipleCreditCardTypeFoundException;
import com.melissa.test.entity.CreditCardType;
import com.melissa.test.exceptions.InvalidCreditCardNumberException;
import com.melissa.test.model.CreditCardModel;
import com.melissa.test.model.CreditCardTypeModel;
import com.melissa.test.repository.CreditCardRepository;
import com.melissa.test.repository.CreditCardTypeRepository;
import com.melissa.test.service.CreditCardService;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "creditCardService")
public class CreditCardServiceImpl implements CreditCardService {

    private static final Logger log = LoggerFactory.getLogger(CreditCardServiceImpl.class);

    @Autowired
    private CreditCardTypeRepository creditCardTypeRepository;
    @Autowired
    private CreditCardRepository creditCardRepository;

    @Override
    public CreditCardTypeModel findCreditCardTypeByNumber(String number) throws Exception {

        log.info("findCreditCardTypeByNumber "+number);

        //get first 6 digits
        CreditCardType creditCardType = getCreditCardTypeByNumber(number);

        return creditCardType.toModel();

    }



    @Override
    public Boolean isCreditCardValid(String number) {

        try {
            sanitizeNumber(number);
        } catch (InvalidCreditCardNumberException e) {
            log.info("isCreditCardValid", number + " : "+ false);
            return false;
        }
        log.info("isCreditCardValid", number + " : "+ true);

        return true;

    }

    @Override
    public CreditCardModel findCreditCardById(String number) throws Exception {
        log.info("findCreditCardById "+number);
        String sanitized = sanitizeNumber(number);

        CreditCard card  = creditCardRepository.findOne(sanitized);
        if(card==null){
            throw new NotFoundException("Credit Card Not Found "+number);
        }

        return card.toModel();
    }

    @Override
    public CreditCardModel createCreditCard(CreditCardModel model) throws Exception {
        if(model==null){
            throw new InvalidCardException();
        }

        String sanitized = sanitizeNumber(model.getCard_number());
        CreditCard card  = creditCardRepository.findOne(sanitized);
        if(card!=null){
            throw  new Exception("Credit Card Already Exists" );
        }

        //get Type

        CreditCardType type = getCreditCardTypeByNumber(model.getCard_number());
        CreditCard creditCard = new CreditCard(model.getCard_number(),model.getCard_holder(),type, model.getNickname());
        creditCard.setId(sanitized);
        creditCard = creditCardRepository.save(creditCard);

        return creditCard.toModel();

    }


    @Override
    public CreditCardModel updateCreditCard(String id, CreditCardModel model) throws Exception {
        if(model==null){
            throw new InvalidCardException();
        }

        String sanitized = sanitizeNumber(model.getCard_number());

        CreditCard card  = creditCardRepository.findOne(sanitized);
        if(card==null){
            throw  new Exception("Credit Card not Found "+id  );
        }

        card.setCardHolder(model.getCard_holder());
        card.setNickname(model.getNickname());


        card = creditCardRepository.save(card);

        return card.toModel();
    }

    @Override
    public Boolean deleteCreditCard(String id) throws Exception {

        String sanitized = sanitizeNumber(id);

        CreditCard card  = creditCardRepository.findOne(sanitized);
        if(card==null){
            throw  new Exception("Credit Card not Found "+id  );
        }

        creditCardRepository.delete(card);
        return true;
    }

    @Override
    public List<CreditCardModel> findCreditCard( String nickName, String cardHolder) throws Exception {

        List<CreditCardModel> creditCardModels = new ArrayList<>();
        if((nickName==null || nickName.isEmpty()) && (cardHolder==null || cardHolder.isEmpty())){
            for (CreditCard creditCard :   creditCardRepository.findAll()) {
                creditCardModels.add(creditCard.toModel());

            }
        }
        else if((nickName!=null && !nickName.isEmpty()) && cardHolder!=null && !cardHolder.isEmpty()) {
            for (CreditCard creditCard :   creditCardRepository.findByNickNameOrCardHolder(nickName, cardHolder)) {
                creditCardModels.add(creditCard.toModel());

            };
        }else if(nickName!=null && !nickName.isEmpty()){
            for (CreditCard creditCard :   creditCardRepository.findByNickName(nickName)) {
                creditCardModels.add(creditCard.toModel());

            }
        }else if(cardHolder!=null && !cardHolder.isEmpty()){
            for (CreditCard creditCard :   creditCardRepository.findByCardHolder(cardHolder)) {
                creditCardModels.add(creditCard.toModel());

            }
        }
        return creditCardModels;
    }


    /**
         * Checks if the credit card number is valid and returns the sanitized version of the number
         * Removes all white spaces and checks if the string is numeric and length >6
         * @param number
         * @return
         * @throws InvalidCreditCardNumberException
         */
    private String sanitizeNumber(String number) throws InvalidCreditCardNumberException{
        log.info("sanitizeNumber "+number);
        if(number == null ) {
            throw new InvalidCreditCardNumberException();
        }

        /**
         * Remove all white spaces
         */
        String sanitizedNumber = number.replaceAll("\\s", "");
        if(sanitizedNumber.isEmpty() || !sanitizedNumber.matches("[0-9]+") || sanitizedNumber.length()<6){
            throw new InvalidCreditCardNumberException(number);
        }
        log.info("sanitizeNumber result  : "+ sanitizedNumber );

        return sanitizedNumber;

    }

    private CreditCardType getCreditCardTypeByNumber(String number) throws Exception{
        String sanitized = sanitizeNumber(number);
        String prefix = sanitized.substring(0,6);
        log.info("getCreditCardTypeByNumber prefix "+prefix);
        List<CreditCardType> list = creditCardTypeRepository.findByNumber(prefix);
        if(list==null || list.isEmpty()){
            throw new CreditCardTypeNotFoundException(number);
        }
        if(list.size()>1){
            throw  new MultipleCreditCardTypeFoundException(number);
        }
        return list.get(0);
    }

    public void setCreditCardTypeRepository(CreditCardTypeRepository creditCardTypeRepository) {
        this.creditCardTypeRepository = creditCardTypeRepository;
    }

    public void setCreditCardRepository(CreditCardRepository creditCardRepository) {
        this.creditCardRepository = creditCardRepository;
    }
}
