package com.melissa.test.service;

import com.melissa.test.exceptions.InvalidCreditCardNumberException;
import com.melissa.test.model.CreditCardModel;
import com.melissa.test.model.CreditCardTypeModel;

import java.util.List;

public interface CreditCardService {

    public CreditCardTypeModel findCreditCardTypeByNumber(String number) throws Exception;
    public Boolean isCreditCardValid(String number);

    public CreditCardModel findCreditCardById(String id) throws Exception;
    public CreditCardModel createCreditCard(CreditCardModel model) throws Exception;
    public CreditCardModel updateCreditCard(String id, CreditCardModel model) throws Exception;
    public Boolean deleteCreditCard(String id) throws Exception;
    public List<CreditCardModel> findCreditCard(String nickName, String cardHolder) throws Exception;

}
