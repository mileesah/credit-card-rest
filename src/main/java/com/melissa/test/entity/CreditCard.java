package com.melissa.test.entity;

import com.melissa.test.model.CreditCardModel;

import javax.persistence.*;

@Entity
@Table(name = "credit_card")
public class CreditCard {

    @Id
    private String id;
    private String cardNumber;
    private String cardHolder;
    @ManyToOne
    @JoinColumn(name="type_id")
    private CreditCardType type;
    @Column(nullable = false)
    private String nickname;

    public CreditCard() {
    }

    public CreditCard(String cardNumber, String cardHolder, CreditCardType type, String nickname) {
        this.cardNumber = cardNumber;
        this.cardHolder = cardHolder;
        this.type = type;
        this.nickname = nickname;
    }

    public CreditCardModel toModel (){
        return new CreditCardModel(this.id, this.cardNumber, this.cardHolder, this.type.getType(), this.type.getSubType(), this.nickname);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public CreditCardType getType() {
        return type;
    }

    public void setType(CreditCardType type) {
        this.type = type;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
