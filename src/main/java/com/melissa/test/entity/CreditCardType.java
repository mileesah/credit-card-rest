package com.melissa.test.entity;

import com.melissa.test.model.CreditCardTypeModel;

import javax.persistence.*;

@Entity
@Table(name = "cc_type")
public class CreditCardType {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String number;
    private String type;
    private String subType;

    public CreditCardType() {
    }

    public CreditCardType(String number, String type, String subType) {
        this.number = number;
        this.type = type;
        this.subType = subType;
    }

    public CreditCardTypeModel toModel(){
        CreditCardTypeModel model = new CreditCardTypeModel(this.type, this.subType);
        return model;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }



    @Override
    public String toString() {
        return "CreditCardType{" +
                "number='" + number + '\'' +
                ", type='" + type + '\'' +
                ", subType='" + subType + '\'' +
                '}';
    }
}
