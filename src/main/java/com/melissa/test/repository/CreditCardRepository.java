package com.melissa.test.repository;

import com.melissa.test.entity.CreditCard;
import com.melissa.test.entity.CreditCardType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CreditCardRepository extends CrudRepository<CreditCard,String> {

    @Query("SELECT c FROM CreditCard c   where  c.nickname like %:nickName% OR c.cardHolder like %:cardHolder%")
    List<CreditCard> findByNickNameOrCardHolder(@Param("nickName") String nickName, @Param("cardHolder")  String cardHolder);

    @Query("SELECT c FROM CreditCard c  where c.nickname like %:nickName% ")
    List<CreditCard> findByNickName( @Param("nickName")  String nickName );

    @Query("SELECT c FROM CreditCard c where c.cardHolder like %:cardHolder% ")
    List<CreditCard> findByCardHolder( @Param("cardHolder")  String cardHolder);


}
