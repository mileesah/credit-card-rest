package com.melissa.test.repository;

import com.melissa.test.entity.CreditCardType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CreditCardTypeRepository extends CrudRepository<CreditCardType,Long> {

    public List<CreditCardType> findByNumber(String number);
}
