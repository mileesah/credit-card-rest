package com.melissa.test.model;

public class CreditCardTypeModel {

    private String type;
    private String subType;

    public CreditCardTypeModel(String type, String subType) {
        this.type = type;
        this.subType = subType;
    }

    public String getType() {
        return type;
    }

    public String getSubType() {
        return subType;
    }
}
