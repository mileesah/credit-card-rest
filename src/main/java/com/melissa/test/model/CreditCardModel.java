package com.melissa.test.model;

import com.melissa.test.entity.CreditCardType;

public class CreditCardModel {

    private String id;
    private String card_number;
    private String card_holder;
    private String type;
    private String subtype;
    private String nickname;

    public CreditCardModel() {
    }

    public CreditCardModel(String id, String card_number, String card_holder, String type, String subtype, String nickname) {
        this.id = id;
        this.card_number = card_number;
        this.card_holder = card_holder;
        this.type = type;
        this.subtype = subtype;
        this.nickname = nickname;
    }

    public CreditCardModel(String card_number, String card_holder, String nickname) {

        this.card_number = card_number;
        this.card_holder = card_holder;
        this.nickname = nickname;
    }


    public String getId() {
        return id;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getCard_holder() {
        return card_holder;
    }

    public void setCard_holder(String card_holder) {
        this.card_holder = card_holder;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "CreditCardModel{" +
                "id=" + id +
                ", card_number='" + card_number + '\'' +
                ", card_holder='" + card_holder + '\'' +
                ", type='" + type + '\'' +
                ", subtype='" + subtype + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
