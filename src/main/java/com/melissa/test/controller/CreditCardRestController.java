package com.melissa.test.controller;


import com.melissa.test.entity.CreditCard;
import com.melissa.test.model.CreditCardModel;
import com.melissa.test.model.CreditCardTypeModel;
import com.melissa.test.service.CreditCardService;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/creditcard")
public class CreditCardRestController {

    private static final Logger logger = LoggerFactory.getLogger(CreditCardRestController.class);

    @Autowired
    CreditCardService creditCardService;



    @RequestMapping(method = RequestMethod.GET, value = "/query/{number}")
    public ResponseEntity<?> getCreditCardType(@PathVariable String number) {
        logger.info("Getting credit card type "+number);
        CreditCardTypeModel model = null;
        try {
            model = creditCardService.findCreditCardTypeByNumber(number);
        } catch (Exception e) {
            Map<String,String> result = new HashMap<>();
            result.put("error", e.getMessage());
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<CreditCardTypeModel>(model, HttpStatus.OK);
    }



    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> findCreditCardById( @RequestParam(name = "nick_name", required = false) String nickName,
                                                     @RequestParam(name = "card_holder", required = false) String cardHolder) {

        List<CreditCardModel> creditCards =null;
        try {

            creditCards = creditCardService.findCreditCard(nickName, cardHolder);
        } catch (NotFoundException e) {
            Map<String,String> result = new HashMap<>();
            result.put("error", e.getMessage());
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.OK);
        }catch (Exception e ){
            Map<String,String> result = new HashMap<>();
            result.put("error", e.getMessage());
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<List<CreditCardModel>>(creditCards, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/new")
    public ResponseEntity<?> createCreditCard(@RequestParam(name = "userId", required = false) String userId, @RequestBody CreditCardModel creditCard) {
        logger.info("Creating Credit Card "+ creditCard.toString());
        CreditCardModel card = null;
        try {
            card = creditCardService.createCreditCard(creditCard);
        } catch (Exception e) {
            Map<String,String> result = new HashMap<>();
            result.put("error", e.getMessage());
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<CreditCardModel>(card, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/update/{credit_card_id}")
    public ResponseEntity<?> updateCreditCard(@RequestParam(name = "userId", required = false) String userId,@PathVariable String credit_card_id, @RequestBody CreditCardModel creditCard) {
        logger.info("Updating Credit Card "+ creditCard.toString());
        CreditCardModel card = null;
        try {
            card = creditCardService.updateCreditCard(credit_card_id,creditCard);
        } catch (Exception e) {
            Map<String,String> result = new HashMap<>();
            result.put("error", e.getMessage());
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<CreditCardModel>(card, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get/{credit_card_id}")
    public ResponseEntity<?> createCreditCardById( @PathVariable String credit_card_id) {

        CreditCardModel creditCard =null;
        try {
            creditCard = creditCardService.findCreditCardById(credit_card_id);
        } catch (NotFoundException e) {
            Map<String,String> result = new HashMap<>();
            result.put("error", e.getMessage());
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.OK);
        }catch (Exception e ){
            Map<String,String> result = new HashMap<>();
            result.put("error", e.getMessage());
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<CreditCardModel>(creditCard, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/delete/{credit_card_id}")
    public ResponseEntity<?> deleteCreditCardById( @PathVariable String credit_card_id) {

        CreditCardModel creditCard =null;
        try {
             creditCardService.deleteCreditCard(credit_card_id);
            Map<String,String> result = new HashMap<>();
            result.put("message", "Credit card "+ credit_card_id+ " deleted");
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.OK);

        } catch (NotFoundException e) {
            Map<String,String> result = new HashMap<>();
            result.put("error", e.getMessage());
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.OK);
        }catch (Exception e ){
            Map<String,String> result = new HashMap<>();
            result.put("error", e.getMessage());
            return new ResponseEntity<Map<String, String>>(result, HttpStatus.BAD_REQUEST);
        }
    }


    public void setCreditCardService(CreditCardService creditCardService) {
        this.creditCardService = creditCardService;
    }

}
