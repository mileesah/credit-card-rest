package com.melissa.test;

import com.melissa.test.entity.CreditCard;
import com.melissa.test.entity.CreditCardType;
import com.melissa.test.repository.CreditCardRepository;
import com.melissa.test.repository.CreditCardTypeRepository;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import org.slf4j.Logger;

@SpringBootApplication
public class CreditCardRestApplication {


	private static final Logger log = LoggerFactory.getLogger(CreditCardRestApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(CreditCardRestApplication.class);
	}



	@Bean
	public CommandLineRunner demo(CreditCardTypeRepository repository, CreditCardRepository creditCardRepository) {
		return (args) -> {
			// save a initial cc types
			//todo generate groovy script
			repository.save(new CreditCardType("455561","VISA", "DEBIT"));
			repository.save(new CreditCardType("387765","AMEX", "CREDIT"));
			repository.save(new CreditCardType("454545","VISA", "CREDIT"));
			repository.save(new CreditCardType("546626","MC", "CREDIT"));


			// fetch all customers
			log.info("CC Type found with findAll():");
			log.info("-------------------------------");
			for (CreditCardType creditCardType : repository.findAll()) {
				log.info(creditCardType.toString());
			}
			log.info("");



			// fetch customers by last name
			log.info("CC Type found with findByNumber('455561'):");
			log.info("--------------------------------------------");
			CreditCardType type = null;
			for (CreditCardType creditCardType : repository.findByNumber("455561")) {
				log.info(creditCardType.toString());
				type = creditCardType;
			}
			log.info("");
			CreditCard creditCard = new CreditCard("455561 7829 232","melissa", type, "card1");
			creditCard.setId("4555617829232");
			creditCardRepository.save(creditCard);
		};
	}

}
