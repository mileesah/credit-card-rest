package com.melissa.test.exceptions;

public class InvalidCreditCardNumberException extends Exception {

    public InvalidCreditCardNumberException() {
        super("Invalid Credit Card Number: Credit card number cannot be empty" );
    }

    public InvalidCreditCardNumberException(String number) {
        super("Invalid Credit Card Number: " + number);
    }
}
