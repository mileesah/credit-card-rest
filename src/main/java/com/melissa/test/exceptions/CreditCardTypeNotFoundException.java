package com.melissa.test.exceptions;

public class CreditCardTypeNotFoundException extends Exception {

    public CreditCardTypeNotFoundException() {
        super("Credit Card Type Not Found");
    }

    public CreditCardTypeNotFoundException(String number) {
        super("Credit Card Type Not Found : "+number);
    }
}
