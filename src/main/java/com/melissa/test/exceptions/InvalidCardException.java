package com.melissa.test.exceptions;

import com.melissa.test.model.CreditCardModel;

public class InvalidCardException extends Exception {



    public InvalidCardException() {
        super("Invalid Card : null");
    }


    public InvalidCardException(CreditCardModel model) {
        super("Invalid Card : "+model.toString());
    }
}
