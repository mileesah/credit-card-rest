package com.melissa.test.exceptions;

public class MultipleCreditCardTypeFoundException extends Exception{

    public MultipleCreditCardTypeFoundException() {
        super("Multiple Credit Card Types Found. Please contact Administrator");
    }

    public MultipleCreditCardTypeFoundException(String number) {
        super("Multiple Credit Card Types Found: "+number+". Please contact Administrator");
    }
}
