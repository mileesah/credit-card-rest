package com.melissa.test.controller;

import com.melissa.test.CreditCardRestApplication;
import com.melissa.test.entity.CreditCardType;
import com.melissa.test.model.CreditCardTypeModel;
import com.melissa.test.repository.CreditCardTypeRepository;
import com.melissa.test.service.CreditCardService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import static org.hamcrest.Matchers.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;


import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = CreditCardRestApplication.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WebAppConfiguration
public class CreditCardRestControllerTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

    }



    @Test
    public void gettingCreditCardType() throws Exception {
        mockMvc.perform(get("/creditcard/query/455561"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.type",  is("VISA")))
                .andExpect(jsonPath("$.subType",  is("DEBIT")));
    }


    @Test
    public void gettingCreditCardType_Invalid() throws Exception {
        mockMvc.perform(get("/creditcard/query/fhdj999"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.error",  startsWith(("Invalid Credit Card Number:"))));
    }

}