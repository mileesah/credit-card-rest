package com.melissa.test.service.impl;

import com.melissa.test.CreditCardRestApplication;
import com.melissa.test.entity.CreditCardType;
import com.melissa.test.exceptions.InvalidCreditCardNumberException;
import com.melissa.test.model.CreditCardTypeModel;
import com.melissa.test.repository.CreditCardRepository;
import com.melissa.test.repository.CreditCardTypeRepository;
import com.melissa.test.service.CreditCardService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)

public class CreditCardServiceImplTest {

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CreditCardTypeRepository creditCardTypeRepository;

    @Autowired
    private CreditCardRepository creditCardRepository;

    @TestConfiguration
    static class CreditCardServiceImplTestConfiguration {
        @Bean
        public CreditCardService creditCardService() {
            return new CreditCardServiceImpl();
        }
        @Bean
        @Primary
        public CreditCardTypeRepository creditCardTypeRepository() {
            return Mockito.mock(CreditCardTypeRepository.class);
        }

        @Bean
        @Primary
        public CreditCardRepository creditCardRepository() {
            return Mockito.mock(CreditCardRepository.class);
        }
    }

    @Test
    public void isCreditCardValid_when_numeric_and_length_gt_6_should_return_True(){
        String number = "123456";
        assertTrue(creditCardService.isCreditCardValid(number));
    }

    @Test
    public void isCreditCardValid_when_numeric_with_spaces_should_return_True(){
        String number = "123 45     6";
        assertTrue(creditCardService.isCreditCardValid(number));
    }


    @Test
    public void isCreditCardValid_when_alphanumeric_with_spaces_should_return_False(){
        String number = "12345ad6";
        assertFalse(creditCardService.isCreditCardValid(number));
    }

    @Test
    public void isCreditCardValid_when_with_symbols_should_return_False(){
        String number = "12345+6";
        assertFalse(creditCardService.isCreditCardValid(number));
    }

    @Test
    public void isCreditCardValid_when_empty_should_return_False(){
        String number = "   ";
        assertFalse(creditCardService.isCreditCardValid(number));
    }

    @Test
    public void isCreditCardValid_when_empty_should_return_False2(){
        String number = "";
        assertFalse(creditCardService.isCreditCardValid(number));
    }

    @Test
    public void isCreditCardValid_when_null_should_return_False(){
        String number = null;
        assertFalse(creditCardService.isCreditCardValid(number));
    }



    @Test
    public void findCreditCardTypeByNumber_when_numeric_should_return_type() throws Exception {
        List<CreditCardType> list = new ArrayList<>();
        list.add(new CreditCardType("455561","VISA", "DEBIT"));

        Mockito.when(creditCardTypeRepository.findByNumber("455561")).thenReturn(list);

        String number = "455561";
        CreditCardTypeModel result = creditCardService.findCreditCardTypeByNumber(number);
        assertNotNull(result);
        assertEquals("VISA", result.getType());
        assertEquals("DEBIT", result.getSubType());

    }


    @Test(expected = InvalidCreditCardNumberException.class)
    public void findCreditCardTypeByNumber_when_alphanumeric_should_return_exception() throws Exception {
        List<CreditCardType> list = new ArrayList<>();
        list.add(new CreditCardType("455561","VISA", "DEBIT"));

        Mockito.when(creditCardTypeRepository.findByNumber("455561")).thenReturn(list);

        String number = "45dfs5561";
        CreditCardTypeModel result = creditCardService.findCreditCardTypeByNumber(number);


    }

    @Test(expected = InvalidCreditCardNumberException.class)
    public void findCreditCardTypeByNumber_when_null_should_return_exception() throws Exception {
        List<CreditCardType> list = new ArrayList<>();
        list.add(new CreditCardType("455561","VISA", "DEBIT"));

        Mockito.when(creditCardTypeRepository.findByNumber("455561")).thenReturn(list);

        String number = null;
        CreditCardTypeModel result = creditCardService.findCreditCardTypeByNumber(number);


    }

    @Test(expected = InvalidCreditCardNumberException.class)
    public void findCreditCardTypeByNumber_when_empty_should_return_exception() throws Exception {
        List<CreditCardType> list = new ArrayList<>();
        list.add(new CreditCardType("455561","VISA", "DEBIT"));

        Mockito.when(creditCardTypeRepository.findByNumber("455561")).thenReturn(list);

        String number = "";
        CreditCardTypeModel result = creditCardService.findCreditCardTypeByNumber(number);


    }



}