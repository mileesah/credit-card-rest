# CREDIT CARD REST API

This is a REST SERVER to store Credit Card Information for a User

## Description

The application allows users to send requests via REST to retrieve Credit Card types, store and update Credit Card information in the database. 
 

### REST Endpoints

* Get Credit Card Type
    * GET http://server:port/creditcard/query/{card_number} 
* Add new Credit Card 
    * POST http://server:port/creditcard/new
 
	```json
       {
         "card_number": "455561 8888 8888",
         "card_holder": "melissaffff",
         "nickname": "card2"
       }
   
    ```

* Update a Credit Card 
    * POST http://server:port/creditcard/update/{card_number}
 
	```json
       {
         "card_number": "455561 8888 8888",
         "card_holder": "melissaffff",
         "nickname": "card2"
       }
   
    ```
* Delete Credit Card 
    * GET http://server:port/creditcard/delete/{card_number} 

* Get Credit Card By ID/Credit Card Number 
    * GET http://server:port/creditcard/delete/{card_number} 
   
* Get Credit Card By nick name and/or card holder
    * GET http://server:port/creditcard?nick_name=<nick_name> 
    * GET http://server:port/creditcard?card_holder=<card_holder> 
    * GET http://server:port/creditcard?nick_name=<nick_name>&card_holder=<card_holder> 

	
  
##TODO:
  
  - Implement UserId for Credit Card ownership
  - Groovy Script
  
  


##Compile, Test, Run and Packaging

- Compile: `mvn compile`

- Test: `mvn test`

- Packaging: `mvn package`, compiled jar in *target/* folder

- Run: `java -jar <compiled jar in target folder> <file path (optional)>`





